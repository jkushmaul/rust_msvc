# Rust docker images

* rust-nightly-linux
* gitlabhelper-windows
* rust-nightly-windows-msvc

All are regularly rebuilt weekly to pull in any updates to their own base images.

## `rust-nightly-linux`

Nothing to see here - just regular rust nightly -

I needed this to use nightly - without havinig cache invalidated nearly every time I build.

* rustup, sscache, cargo-bintools are pre-installed, as well as grcov, tarpaulin + rustup component llvm-tools-preview for coverage

There is nothing special to running this, run/use it as you would any rust image.

```yaml
    image: registry.gitlab.com/jkushmaul/rust_docker/rust-nightly-linux:latest
    variables:
        CARGO_HOME: ${CI_PROJECT_DIR}/cargo
    cache:
        paths:
            - ${CI_PROJECT_DIR}/cargo
    script:
        - cargo build
        - grcov ...
```

## `gitlabhelper-windows`

nano based image for starters.  And it also sets a git config that gitlab probably doesn't know about that their windows helpers have when dealing with hyperV isolation docker containers,
regarding git safe directory...

```toml
  shell = "pwsh"
  [runners.docker]
    helper_image = "registry.gitlab.com/jkushmaul/rust_docker/gitlabhelper-windows:latest"
```

## `rust-nightly-windows-msvc`

This is a nano based image and DOES NOT provide MSVC/Windows SDK - those must be preinstalled and mounted.

Ever try to build rust on windows in docker?  It's not fun.  Nor is the resulting image sizes to package everything so this is, I hope, a happy medium but it does require
having pre-installed tools and some light setup.

### Running the image

This one is not quite so simiple...

1. Manual
2. Helper powershell script `docker_run.ps1`

#### Manual

Extract docker_run.ps1 and run it.  it will find msvc, windows sdk, and init the shell with these vars
then invoke docker run with all the right settings for current working dir.  You just pass the command to run to it.

All the `docker_run.ps1` is doing is ultimately, best-attempt trying to populate the vars and mounts for this

```ps
docker run --rm
    -e VSCMD_ARG_HOST_ARCH=`"${ENV:VSCMD_ARG_HOST_ARCH}`"
    -e VSCMD_ARG_TGT_ARCH=`"${ENV:VSCMD_ARG_TGT_ARCH}`"
    -e WindowsSDKLibVersion=`"${ENV:WindowsSDKLibVersion}`"
    -v `"${Env:VCToolsInstallDir}:C:/VCToolsInstallDir:ro`"
    -v `"${Env:WindowsSdkDir}:C:/WindowsSdkDir:ro`"
    -v `"${pwd}:C:/workdir`"
    -w 'C:/workdir' $args
```

So in order to run manually you need

1. Pre-installed vctools, bind-mount to `C:/VCToolsInstallDir`
2. Pre-installed windows sdk, bind-mount to `C:/WindowsSdkDir`
3. Provide `WindowsSDKLibVersion` env
4. Provide `VSCMD_ARG_HOST_ARCH` env (commonly x64)
5. Provide `VSCMD_ARG_TGT_ARCH` env (commonly x64)

To aquire the paths to those bind-mounts, the `x64 Native Tools Command Prompt for VS...` can be used and the env vars are directly there to pass through, or just copy them if you
cannot run that powershell.

The docker container entry point will look into these dirs, and using these env vars, update path so that link.exe is resolvable.

```yaml
variables:
    CARGO_HOME: ${CI_PROJECT_DIR}/cargo
    VCToolsInstallDir: 'C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Tools\MSVC\14.29.30133'
    WindowsSdkDir: 'C:\Program Files (x86)\Windows Kits\10:C:\WindowsSdkDir:ro'
    WindowsSDKLibVersion: '10.0.19041.0'
    VSCMD_ARG_HOST_ARCH: 'x64'
    VSCMD_ARG_TGT_ARCH: 'x64'
script:
    - >
       $RUN="docker run --rm
        -e VSCMD_ARG_HOST_ARCH=`"${ENV:VSCMD_ARG_HOST_ARCH}`"
        -e VSCMD_ARG_TGT_ARCH=`"${ENV:VSCMD_ARG_TGT_ARCH}`"
        -e WindowsSDKLibVersion=`"${ENV:WindowsSDKLibVersion}`"
        -v `"${Env:VCToolsInstallDir}:C:/VCToolsInstallDir:ro`"
        -v `"${Env:WindowsSdkDir}:C:/WindowsSdkDir:ro`"
        -v `"${pwd}:C:/workdir`"
        -w 'C:/workdir' $args"
    - $RUN registry.gitlab.com/jkushmaul/rust_msvc cargo init
    - $RUN registry.gitlab.com/jkushmaul/rust_msvc cargo build
```

At which point then your gitlab-ci yaml can just use the image directly, because the mounts + env vars are already setup:

```yaml
image: registry.gitlab.com/jkushmaul/rust_msvc:rust-latest
variables:
    CARGO_HOME: ${CI_PROJECT_DIR}/cargo
script:
    - cargo init
    - cargo build
```

#### Via Helper script: `docker_run.ps1`

A convenience script is contained within the root of the image to aid in the startup.

1. Copy the file out of the container
2. Run the script

```yaml
variables:
    CARGO_HOME: ${CI_PROJECT_DIR}/cargo
script:
    - docker run --rm -v ${pwd}:C:\workdir -w C:\workdir --entrypoint=powershell registry.gitlab.com/jkushmaul/rust_msvc
        "Copy-Item C:\docker_run.ps1 C:\workdir\docker_run.ps1"
    - powershell -File docker_run.ps1 registry.gitlab.com/jkushmaul/rust_msvc cargo init
    - powershell -File docker_run.ps1 registry.gitlab.com/jkushmaul/rust_msvc cargo build
```

1. `C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe` is used to determine the install path for msvc; `${Env:VCToolsInstallDir}` skips this step
2. `C:\Program Files (x86)\Windows Kits\10` is used to find the windows SDK verison etc; `${$ENV:WindowsSdkDir}` skips this step - and also requires the other vars.

I wrote this thinking it would make it easier but after looking on it and manually setting up the runner etc, I think manual is easier to understand now.  Leaving it here if it helps.

## Licensing

All attempts were made to keep this risk free on licensing, despite having to depend on microsoft for nano, powershell, and gitlab runner binaries
