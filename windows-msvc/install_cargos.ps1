
function Install-Cargo {
    param (
        $GithubProject,
        $ArchiveFilenameTemplate,
        $InternalFilename
    )

    # I can't get binstall to work on windows nano image...  Wonder what else doesn't work.
    $url = "https://github.com/$GithubProject/releases/latest"
    write-host "Latest URL: $url"
    $LATESTV=(Invoke-WebRequest -ErrorAction:SilentlyContinue  -SkipHttpErrorCheck  -UseBasicParsing -Uri  $url   -MaximumRedirection 0).Headers.Location -Split '/' | Select-Object -Last 1
    $ArchiveFilename = "$($ExecutionContext.InvokeCommand.ExpandString($ArchiveFilenameTemplate))"
    $InternalFilename = "$($ExecutionContext.InvokeCommand.ExpandString($InternalFilename))"
    $DownloadUrl =  "https://github.com/${GithubProject}/releases/download/${LATESTV}/${ArchiveFilename}"
    write-host "LATESTV: $LATESTV; ArchiveFilename: $ArchiveFilename; InternalFilename: $InternalFilename; DownloadUrl: $DownloadUrl"

    if (Test-Path C:/outfile.zip) {
        Remove-Item C:/outfile.zip -Force -Recurse
    }
    if (Test-Path C:/outfile) {
        Remove-Item C:/outfile -Force -Recurse
    }

    Invoke-WebRequest -Uri $DownloadUrl -OutFile C:/outfile.zip
    Expand-Archive -Path C:/outfile.zip  C:/outfile/


    Move-Item C:/outfile/$InternalFilename C:/cargo/bin/
    Remove-Item -Recurse -Force C:/outfile*
}

Install-Cargo -GithubProject "mozilla/grcov" -ArchiveFilenameTemplate "grcov-x86_64-pc-windows-msvc.zip" -InternalFilename "grcov.exe"
grcov.exe --version

Install-Cargo  -GithubProject "mozilla/sccache" -ArchiveFilenameTemplate 'sccache-${LATESTV}-x86_64-pc-windows-msvc.zip' -InternalFilename 'sccache-${LATESTV}-x86_64-pc-windows-msvc/sccache.exe'
sccache.exe --version

Install-Cargo -GithubProject "xd009642/tarpaulin" -ArchiveFilenameTemplate 'cargo-tarpaulin-x86_64-pc-windows-msvc.zip' -InternalFilename "cargo-tarpaulin.exe"
cargo.exe tarpaulin --version

Install-Cargo -GithubProject "rust-embedded/cargo-binutils" -ArchiveFilenameTemplate 'x86_64-pc-windows-msvc.zip' -InternalFilename "x86_64-pc-windows-msvc/*.exe"
cargo.exe cov --version

Install-Cargo -GithubProject "nextest-rs/nextest" -ArchiveFilenameTemplate '${LATESTV}-x86_64-pc-windows-msvc.zip' -InternalFilename "cargo-nextest.exe"
cargo.exe nextest -V
