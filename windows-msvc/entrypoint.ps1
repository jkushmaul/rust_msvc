#$cmd = $args
#$ErrorActionPreference = 'Stop'

Trap {
    Write-Host "**************** ERROR **********************"
    Get-Item -Path "Env:" | Sort-Object  | Format-Table -Wrap -AutoSize
    Write-Host "----------------"
    Write-Host "Error: $_"
    break
}

if ($Env:PSDebug) {
    Set-PSDebug -Trace 1
    $DebugPreference = "Continue"
}


function get_env {
    param($Name)

    $Value = Get-Item "Env:$Name" -ErrorAction SilentlyContinue
    if ($Value -eq $null) {
        $Value = ""
    } else {
        $Value = $Value.Value
    }
    $Value
}

# Word on the street is that setting via $Env:VAR=VAL doesn't actually set anything outside of the scope of this script.
# Since this is a  bootstrap of the env, I want it to just outright set it.  It might be incorrect but seems right as of now.
# For instance, if I ran bootstrap from CMD shell - I'd lose my bootstrapped vars. I'd always have to invoke from this script to retain
# Which I do - so it's not a problem the way I have it written now, but I'm not a fan of it and it might change. This future proofs that part.
function set_env {
    param(
        [string] $Name,
        [string] $Value,
        [switch] $Append
        )

    if ($Append) {
        $prev = get_env -Name $Name
        if ($prev) {
            $Value = "$prev;$Value"
        }
    }
    [Environment]::SetEnvironmentVariable("$Name", "$Value")
}

function require_var {
    param($Name, $ErrorMessage)

    $val = get_env -Name $Name

    if ("$val" -eq "") {
        throw "Env:${Name} did not exist; $ErrorMessage"
    }

}

function require_container_var {
    param($Name)

    (require_var -Name $Name -ErrorMessage "$Name missing - build issue; it should be defined in docker build" )
}


function require_host_var {
    param($Name)

    (require_var -Name $Name -ErrorMessage "$Name missing - run issue; it should be specified on docker run")
}



function require_file {
    param($Path, $ErrorMessage)


    if (-not (Test-Path $Path -PathType Leaf)) {
        throw "Missing file: $Path; ${ErrorMessage}"
    }
}

function require_dir {
    param($Path, $ErrorMessage)


    if (-not (Test-Path $Path -PathType Container)) {
        throw "Missing mount: $Path;  ${ErrorMessage}"
    }
}


require_container_var -Name 'VCToolsInstallDir'
require_container_var -Name 'WindowsSdkDir'
require_host_var -Name 'VSCMD_ARG_HOST_ARCH'
require_host_var -Name 'VSCMD_ARG_TGT_ARCH'
require_host_var -Name 'WindowsSDKLibVersion'


require_dir -Path ${Env:VCToolsInstallDir} -ErrorMessage "Should be mounting VcTools, eg) -v 'C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.32.31326\:C:\VCToolsInstallDir'"
require_dir -Path ${Env:WindowsSdkDir} -ErrorMessage "Should be mounting windows sdk, eg) -v 'C:\Program Files (x86)\Windows Kits\10\:WindowsSdkDir'"


#VSCMD_ARG_TGT_ARCH=x64
#VSCMD_ARG_HOST_ARCH=x64
# For link.exe
#VCToolsInstallDir=C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.32.31326\
$LINK_EXE_PATH = "${Env:VCToolsInstallDir}\bin\Host${Env:VSCMD_ARG_HOST_ARCH}\${Env:VSCMD_ARG_TGT_ARCH}"
require_file -Path "$LINK_EXE_PATH\link.exe"
set_env -Append -Name "PATH" -Value $LINK_EXE_PATH
Get-Command link.exe | Out-Null

# for msvcrt.lib
#C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\MSVC\14.32.31326\lib\x64
$MSVCRT_PATH="${Env:VCToolsInstallDir}\lib\${Env:VSCMD_ARG_TGT_ARCH}"
require_file -Path "${MSVCRT_PATH}\msvcrt.lib"
set_env -Name "LIB" -Append -Value $MSVCRT_PATH



#WindowsSDKLibVersion=10.0.19041.0\
# for kernel32.lib
$UM_PATH = "${Env:WindowsSDKDir}\lib\${Env:WindowsSDKLibVersion}\um\${Env:VSCMD_ARG_TGT_ARCH}"
require_file -Path "$UM_PATH\kernel32.lib"
set_env -Name "LIB" -Append -Value $UM_PATH

# for ucrt.lib
$UCRT_PATH = "${Env:WindowsSDKDir}\lib\${Env:WindowsSDKLibVersion}\ucrt\${Env:VSCMD_ARG_TGT_ARCH}"
require_file -Path "$UCRT_PATH\ucrt.lib"
set_env -Name "LIB" -Append -Value $UCRT_PATH


set_env -Name "LIBPATH" -Value "$LIB"


set_env -Name "INCLUDE" -Append -Value "${Env:VCToolsInstallDir}\ATLMFC\include"
set_env -Name "INCLUDE" -Append -Value "${Env:VCToolsInstallDir}\include"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\${Env:WindowsSDKLibVersion}\ucrt"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\${Env:WindowsSDKLibVersion}\shared"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\include\${Env:WindowsSDKLibVersion}\um"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\include\${Env:WindowsSDKLibVersion}\ucrt"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\include\${Env:WindowsSDKLibVersion}\shared"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\include\${Env:WindowsSDKLibVersion}\winrt"
set_env -Name "INCLUDE" -Append -Value "${Env:WindowsSDKDir}\include\${Env:WindowsSDKLibVersion}\cppwinrt"


#find  /cygdrive/c/Program\ Files\ \(x86\)/Windows\ Kits/ -name stddef.h
#/cygdrive/c/Program Files (x86)/Windows Kits/10/Include/10.0.19041.0/ucrt/stddef.h


#if ($cmd) {
#    Invoke-Expression "$cmd"
#}
