$cmd = $args

$ErrorActionPreference = 'Stop'

if ($Env:PSDebug) {
    Set-PSDebug -Trace 1
    $DebugPreference = "Continue"
}

Trap {
    $e = @"
    **************** ERROR **********************
    ${Get-Item -Path "Env:" | Sort-Object  | Format-Table -Wrap -AutoSize}
    ----------------
    Error: $_
"@
    throw $e
}


if ("$ENV:VCToolsInstallDir" -eq "") {
    $PROPS = @{}

    & "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe" | ForEach-Object {
        $parts = $_ -Split ': '
        $Name = $parts[0];
        $Value = $parts[1];
        $PROPS[$Name]=$Value
    }

    $installationPath=$PROPS["installationPath"]

    $VCToolsInstallDir="$installationPath\VC\Tools\MSVC"


    $VCVERS = Get-ChildItem -Path $VCToolsInstallDir -Filter "20*" | Sort-Object -Property Name -Descending


    $VCToolsVersion=$VCVERS[0]

    $VCToolsInstallDir = "${VCToolsInstallDir}\$VCToolsVersion"

    Set-Item -Path "Env:VCToolsInstallDir" -Value "$VCToolsInstallDir" -Force
    Set-Item -Path "Env:VCToolsVersion" -Value "$VCToolsVersion" -Force
}

if ("$ENV:WindowsSdkDir" -eq "") {

    $WindowsSdkDir='C:\Program Files (x86)\Windows Kits\10'
    $WindowsSdkVerBinPath="$WindowsSdkDir\bin"

    $WSDKVERS = Get-ChildItem -Path $WindowsSdkVerBinPath -Filter "10.*" |  Sort-Object -Property Name -Descending

    $WindowsSDKLibVersion=$WSDKVERS[0]
    $WindowsSDKVersion=$WindowsSDKLibVersion

    Set-Item -Path "Env:WindowsSdkDir" -Value "$WindowsSdkDir" -Force
    Set-Item -Path "Env:WindowsSdkVerBinPath" -Value "$WindowsSdkVerBinPath" -Force
    Set-Item -Path "Env:WindowsSDKLibVersion" -Value "$WindowsSDKLibVersion" -Force
    Set-Item -Path "Env:WindowsSDKVersion" -Value "$WindowsSDKLibVersion" -Force
}


if ("$Env:VSCMD_ARG_HOST_ARCH" -eq "") {
    $VSCMD_ARG_HOST_ARCH = Switch ($Env:PROCESSOR_ARCHITECTURE) {
            "AMD64" { "x64" }
            "ARM64" {"arm64"}
            "x86" {"x86" }
        }


    # Export
    Set-Item -Path "Env:VSCMD_ARG_HOST_ARCH" -Value "$VSCMD_ARG_HOST_ARCH" -Force
    #it is intended to pass through previous var,  HOST, not passthrough this variable named TGT
    Set-Item -Path "Env:VSCMD_ARG_TGT_ARCH" -Value "$VSCMD_ARG_HOST_ARCH" -Force
}


if ($Env:INTERACTIVE) {
    $INTERACTIVE="-i"
}
if ($Env:TTY) {
    $TTY="-t"
}


$CargoHomeMount=""
if ($Env:CARGO_HOME) {
    if ("${Env:CARGO_HOME}".Replace("\", "/").StartsWith("C:/workdir/")) {
        $CargoHomeMount="-v `"${Env:CARGO_HOME}:C:/cargo-home`""
    }
} else {
    $Env:CARGO_HOME="C:\workdir\cargo-home"
}


$Expression = "docker run --rm $INTERACTIVE $TTY $CargoHomeMount -e CARGO_HOME=`"${ENV:CARGO_HOME}`" -e VSCMD_ARG_HOST_ARCH=`"${ENV:VSCMD_ARG_HOST_ARCH}`" -e VSCMD_ARG_TGT_ARCH=`"${ENV:VSCMD_ARG_TGT_ARCH}`" -e WindowsSDKLibVersion=`"${ENV:WindowsSDKLibVersion}`"   -v `"${Env:VCToolsInstallDir}:C:/VCToolsInstallDir:ro`" -v `"${Env:WindowsSdkDir}:C:/WindowsSdkDir:ro`"  -v `"${pwd}:C:/workdir`" -w 'C:/workdir' $cmd"

Write-Debug "Invoke-Expression $Expression"

Invoke-Expression $Expression
