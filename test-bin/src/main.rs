use futures;

fn main() {
    let h = futures::executor::block_on(async move { hello().await });
    print!("{}", h);
}

async fn hello() -> String {
    return String::from("Hello!");
}

#[cfg(test)]
mod test {
    #[test]
    pub fn test_hello() {
        let actual = futures::executor::block_on(async move { crate::hello().await });
        let expected = "Hello!";
        assert_eq!(actual.as_str(), expected);
    }
}
